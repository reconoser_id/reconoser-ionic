import { Component, NgZone } from '@angular/core';
import { LoadingController } from '@ionic/angular';

/**
 *Interface para la variable metodo
 *
 * @interface Method
 */
interface Method {
  id: number;
  description: string
}

/**
 * Clase para la vista Preguntas
 *
 * @export
 * @class QuestionsPage
 */
@Component({
  selector: 'app-questions',
  templateUrl: './questions.page.html',
  styleUrls: ['./questions.page.scss'],
})
export class QuestionsPage {
  // Declarar variable method
  public selectedMethod: Method = {
    id: 1,
    description: 'MOSTRAR PREGUNTAS',
  };
  // Declarar variable guidCiu
  public guidCiu: string = "3f200221-b9f8-4767-9a57-017974aa8b64";
  // Declarar variable response
  public response: string = "";
  // Declarar variable loading
  public loading;
  // Declarar variable methods
  public methods: any[] = [
    {
      id: 1,
      description: 'MOSTRAR PREGUNTAS',
    },
    {
      id: 2,
      description: 'SOLICITAR PREGUNTAS',
    },
    {
      id: 3,
      description: 'VALIDAR PREGUNTAS',
    },
  ];

  /**
   *Creates an instance of QuestionsPage.
   * @param {NgZone} zone
   * @memberof QuestionsPage
   */
  constructor(private zone: NgZone, private loadingController: LoadingController) {}

  /**
   *Función para validar cuando se habilita el botón guardar
   * @example
   * Simply call: 
   * validateButton()
   *
   * @memberof QuestionsPage
   */
  validateButton(){
    switch (this.selectedMethod.id) {
      case 1:
        return this.guidCiu.length === 0;
      default:
        return false;
    }
  }

  /**
   * Llama a las funciones de guardar según método
   * 
   * @example
   * Simply call: 
   * callRequest()
   * 
   * @memberof QuestionsPage
   */
  callRequest() {
    switch (this.selectedMethod.id) {
      case 1:
        this.callMostrarPreguntas();
        break;
      case 2:
        this.callSolicitarPreguntasDemograficas();
        break;
      case 3:
        this.callValidarRespuestaDemograficas();
        break;
    }
  }

  /**
   * Llama a las funciones de mostrar preguntas
   * 
   * @example
   * Simply call: 
   * callMostrarPreguntas()
   * 
   * @memberof QuestionsPage
   */
  async callMostrarPreguntas(){
    await this.presentLoading();
    this.response = "";
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.mostrarPreguntas(
      [this.guidCiu],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Llama a las función de solicitar preguntas demográficas
   * 
   * @example
   * Simply call: 
   * callSolicitarPreguntasDemograficas()
   * 
   * @memberof QuestionsPage
   */
  async callSolicitarPreguntasDemograficas(){
    await this.presentLoading();
    this.response = "";
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.solicitarPreguntasDemograficas(
      [this.guidCiu],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Llama a las función de validar respuestas demográficas
   * 
   * @example
   * Simply call: 
   * callValidarRespuestaDemograficas()
   * 
   * @memberof QuestionsPage
   */
  async callValidarRespuestaDemograficas(){
    if(this.response.length === 0){
      alert('Debe haber solicitado primero las preguntas'); 
      return;
    }

    try {
      let res = JSON.parse(this.response);
      let respuestas = [];
  
      if(!res.Preguntas || (res.Preguntas && res.Preguntas.length === 0)){
        alert('No se encontraron las preguntas en la respuesta'); 
        return;
      }

      await this.presentLoading();
      var idPregunta;
      for (let i = 0; i < res.Preguntas.length; i++) {
        if(res.Preguntas[i].IdPregunta){ // Android
          idPregunta = res.Preguntas[i].IdPregunta;
        }else{ // iOS
          idPregunta = res.Preguntas[i].idPregunta;
        }
        respuestas.push({"idPregunta": String(idPregunta), "idRespuesta": "001"});
        // respuestas.push({"idPregunta": String(res.Preguntas[i].IdPregunta), "idRespuesta": String(res.Preguntas[i].OpcionesRespuestas[i].IdRespuesta)}); // Android: OpcionesRespuestas
        // respuestas.push({"idPregunta": String(res.Preguntas[i].IdPregunta), "idRespuesta": String(res.Preguntas[i].opcionesRespuestas[i].IdRespuesta)}); // iOS: opcionesRespuestas
      }
      var idCuestionario;
      if(res.IdCuestionario){ // Android
        idCuestionario = String(res.IdCuestionario);
      }else{ // iOS
        idCuestionario = String(res.idCuestionario);
      }
      var registroCuestionario;
      if(res.RegistroCuestionario){ // Android
        registroCuestionario = String(res.RegistroCuestionario);
      }else{ // iOS
        registroCuestionario = String(res.registroCuestionario);
      }
      // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
      (<any>window).ReconoSerSdk.validarRespuestaDemograficas(
        [this.guidCiu, idCuestionario, registroCuestionario, respuestas],
        this.successCallback.bind(this),
        this.errorCallback.bind(this)
      );
    } catch (error) {
      alert(error)
    }
  }

  /**
   *Función para mostrar inicialiar loading
   * @example
   * Simply call: 
   * presentLoading()
   *
   * @memberof BiometricsPage
   */
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Espere...',
    });
    await this.loading.present();
  }

  /**
   *Función para terminar loading
   * @example
   * Simply call: 
   * presentLoading()
   *
   * @memberof BiometricsPage
   */
  async dismissLoading() {
    await this.loading.dismiss();
  }

  /**
   * Funcion para recibir las respuestas exitosas del SDK
   * 
   * @example
   * Simply call it with an object: 
   * successCallback({response, true})
   * 
   * @param {object} message
   * @memberof BiometricsPage
   */
  async successCallback(message) {
    this.zone.run(() => {
      if(typeof(message) === "object"){
        this.response = JSON.stringify(message);
      }else{
        this.response = message;
      }
    });
    await this.dismissLoading()
  }

  /**
   * Funcion para recibir las respuestas fallidas del SDK
   * 
   * @example
   * Simply call it with an object: 
   * errorCallback({error, true})
   * 
   * @param {object} err
   * @memberof BiometricsPage
   */
  async errorCallback(err) {
    this.zone.run(() => {
      if(typeof(err) === "object"){
        this.response = JSON.stringify(err);
      }else{
        this.response = err;
      }
    });
    await this.dismissLoading()
  }

  /**
   * Funcion para comparar objectos en selector
   * 
   * @example
   * Simply call with two arguments
   * compareFn(method1, method2)
   * 
   * @param {object} err
   * @memberof QuestionsPage
   * @returns boolean 
   */
  compareFn(e1: Method, e2: Method): boolean {
    return e1 && e2 ? e1.id == e2.id : e1 == e2;
  }

}
