import { Component, NgZone } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { DomSanitizer } from '@angular/platform-browser';

/**
 *Interface para la variable metodo
 *
 * @interface Method
 */
interface Method {
  id: number;
  description: string
}

/**
 *Interface para la variable Image
 *
 * @interface Image
 */
interface Image {
  path: any;
  pathFile: string
}

/**
 * Clase para la vista Biometria
 *
 * @export
 * @class BiometricsPage
 */
@Component({
  selector: 'app-biometrics',
  templateUrl: './biometrics.page.html',
  styleUrls: ['./biometrics.page.scss'],
})
export class BiometricsPage {
  // Declarar variable method
  public selectedMethod: Method = {
    id: 1,
    description: 'ANVERSO',
  };
  // Declarar variable guidConv
  public guidConv: string = "BFA3AB5F-BAEB-42DE-986E-0DA98F45F90F"
  // Declarar variable guidCiu
  public guidCiu: string = "3f200221-b9f8-4767-9a57-017974aa8b64";
  // Declarar variable dataAdi
  public dataAdi: string = "";
  // Declarar variable image
  public image: Image;
  // Declarar variable formato
  public formato: string = "JPG_B64";
  // Declarar variable usuario
  public usuario: string = "test";
  // Declarar variable response
  public response: string = "";
  // Declarar variable loading
  public loading;
  // Declarar variable methods
  public methods: any[] = [
    {
      id: 1,
      description: 'ANVERSO',
    },
    {
      id: 2,
      description: 'REVERSO',
    },
    {
      id: 3,
      description: 'BARCODE',
    },
    {
      id: 4,
      description: 'BIOMETRÍA',
    },
    // {
    //   id: 5,
    //   description: 'VALIDACIÓN BIOMETRICA',
    // },
  ];

  /**
   *Creates an instance of BiometricsPage.
   * @param {NgZone} zone
   * @memberof BiometricsPage
   */
  constructor(private zone: NgZone, private loadingController: LoadingController, private webview: WebView, private sanitizer: DomSanitizer) { }

  /**
   *Función para validar cuando se habilita el botón guardar
   * @example
   * Simply call: 
   * validateButton()
   *
   * @memberof ServicesPage
   */
  validateButton(){
    switch (this.selectedMethod.id) {
      case 1:
        return this.guidCiu.length === 0 || !this.image || (this.image && !this.image['path']);
      case 2:
        return this.guidCiu.length === 0 || !this.image || (this.image && !this.image['path']);
      case 3:
        return this.guidCiu.length === 0 || this.response.length === 0;
      case 4:
        return this.guidCiu.length === 0 || !this.image || (this.image && !this.image['path']);
      // case 5:
      //   return this.guidCiu.length === 0 || !this.image || (this.image && !this.image['path']);
      default:
        return false;
    }
  }

  /**
   *Llama a las funciones de capturar según método
   * @example
   * Simply call: 
   * callTake()
   *
   * @memberof BiometricsPage
   */
  callTake() {
    switch (this.selectedMethod.id) {
      case 1:
        this.callAnversoCapturar();
        break;
      case 2:
        this.callReversoCapturar();
        break;
      case 3:
        this.callBarcodeCapturar();
        break;
    }
  }

  /**
   *Llama a las función de capturar anverso
   * @example
   * Simply call: 
   * callAnversoCapturar()
   *
   * @memberof BiometricsPage
   */
  async callAnversoCapturar(){
    await this.presentLoading();
    this.response = "";
    this.image = null;
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.anversoCapturar(
      [this.guidCiu, this.dataAdi, this.guidConv],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   *Llama a las función de capturar reverso
   * @example
   * Simply call: 
   * callReversoCapturar()
   *
   * @memberof BiometricsPage
   */
  async callReversoCapturar(){
    await this.presentLoading();
    this.response = "";
    this.image = null;
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.reversoCapturar(
      [this.guidCiu, this.dataAdi, this.guidConv],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Función para validar la biometria facial contra la foto anverso
   * 
   * @example
   * Simply call: 
   * callBarcodeCapturar()
   * 
   * @memberof BiometricsPage
   */
  async callBarcodeCapturar() {
    await this.presentLoading();
    this.response = "";
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.barcodeCapturar(
      [this.guidCiu, this.dataAdi, this.guidConv],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Llama a las funciones de guardar biometría
   * 
   * @example
   * Simply call: 
   * callSaveBiometria()
   * 
   * @memberof BiometricsPage
   */
  async callSaveBiometria(idServicio: string, subtipo: string){
    await this.presentLoading();
    this.response = "";
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.guardarBiometria(
      [this.guidCiu, idServicio, subtipo, this.image.pathFile, this.formato, this.dataAdi, this.usuario],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Llama a las funciones de guardar documento
   * 
   * @example
   * Simply call: 
   * callGuardarDocumento()
   * 
   * @memberof BiometricsPage
   */
  async callGuardarDocumento(){
    await this.presentLoading();
    let res = JSON.parse(this.response);
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.guardarDocumento(
      [this.guidCiu, res.result],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Función para validar la biometria facial contra la foto anverso
   * 
   * @example
   * Simply call: 
   * callGuardarBiometriaReferencia()
   * 
   * @memberof BiometricsPage
   */
  async callGuardarBiometriaReferencia() {
    await this.presentLoading();
    this.response = "";
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.guardarBiometriaReferencia(
      [this.guidCiu, this.image.pathFile, this.guidConv, this.dataAdi],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Función para validar la biometria facial contra la foto anverso
   * 
   * @example
   * Simply call: 
   * callValidarBiometria()
   * 
   * @memberof BiometricsPage
   */
  async callValidarBiometria() {
    await this.presentLoading();
    this.response = "";
    // Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente    
    (<any>window).ReconoSerSdk.validarBiometria(
      [this.guidCiu, this.image.pathFile, this.formato, "5"],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   * Llama a las funciones de guardar según método
   * 
   * @example
   * Simply call: 
   * callSave()
   * 
   * @memberof BiometricsPage
   */
  callSave() {
    switch (this.selectedMethod.id) {
      case 1:
        this.callSaveBiometria("4", "Anverso");
        break;
      case 2:
        this.callSaveBiometria("4", "Reverso");
        break;
      case 3: 
        this.callGuardarDocumento();
        break;
      case 4:
        // this.callSaveBiometria( "5", "Frontal");
        this.callGuardarBiometriaReferencia();
        break;
    }
  }

  /**
   * Función de referencia del método biometria
   * 
   * @example
   * Simply call: 
   * callReference()
   * 
   * @memberof BiometricsPage
   */
  callReference() {
    this.callAnversoCapturar();
  }

  /**
   * Función para validar la biometria facial contra foto en servidor
   * 
   * @example
   * Simply call: 
   * callValidate()
   * 
   * @memberof BiometricsPage
   */
  callValidate() {
    this.callValidarBiometria();
  }

  /**
   *Función para mostrar inicialiar loading
   * @example
   * Simply call: 
   * presentLoading()
   *
   * @memberof BiometricsPage
   */
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Espere...',
    });
    await this.loading.present();
  }

  /**
   *Función para terminar loading
   * @example
   * Simply call: 
   * presentLoading()
   *
   * @memberof BiometricsPage
   */
  async dismissLoading() {
    await this.loading.dismiss();
  }

  /**
   * Funcion para recibir las respuestas exitosas del SDK
   * 
   * @example
   * Simply call it with an object: 
   * successCallback({response, true})
   * 
   * @param {object} message
   * @memberof BiometricsPage
   */
  async successCallback(message) {
    this.zone.run(() => {
      if(message.path_file_r){
        let resPath = this.pathForImage(message.path_file_r);
        this.image = { path: resPath, pathFile: message.path_file_r };
      }
      if(typeof(message) === "object"){
        this.response = JSON.stringify(message);
      }else{
        this.response = message;
      }
    });
    await this.dismissLoading()
  }

  /**
   * Funcion para recibir las respuestas fallidas del SDK
   * 
   * @example
   * Simply call it with an object: 
   * errorCallback({error, true})
   * 
   * @param {object} err
   * @memberof BiometricsPage
   */
  async errorCallback(err) {
    this.zone.run(() => {
      if(typeof(err) === "object"){
        this.response = JSON.stringify(err);
      }else{
        this.response = err;
      }
    });
    await this.dismissLoading()
  }

  /**
   * Funcion para parsear path to src
   * 
   * @example
   * Simply call it with an object: 
   * errorCallback(path)
   * 
   * @param {object} err
   * @memberof BiometricsPage
   * @returns string
   */
  pathForImage(path) {
    if (path === null) {
      return '';
    } else {
      const resolvedImg = this.webview.convertFileSrc(path);
      const safeImg = this.sanitizer.bypassSecurityTrustUrl(resolvedImg);
      return safeImg;
    }
  }

  /**
   * Funcion para comparar objectos en selector
   * 
   * @example
   * Simply call with two arguments
   * compareFn(method1, method2)
   * 
   * @param {object} err
   * @memberof BiometricsPage
   * @returns boolean 
   */
  compareFn(e1: Method, e2: Method): boolean {
    return e1 && e2 ? e1.id == e2.id : e1 == e2;
  }
}
