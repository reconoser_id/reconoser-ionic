import { Component } from "@angular/core";
//Agregar Platform para poder evaluar si ya se cargo y esta lista la plataforma
import { NavController, Platform } from "@ionic/angular";

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  constructor(public navCtrl: NavController, public platform: Platform, public androidPermissions: AndroidPermissions) {
    //Verifica si ya se encuentra lista la plataforma
    this.platform.ready().then(() => {
      //Solicitar los permisos para la camara
      this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    });
  }
}
