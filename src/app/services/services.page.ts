import { Component, NgZone } from '@angular/core';
import { LoadingController } from '@ionic/angular';

/**
 *Interface para la variable metodo
 *
 * @interface Method
 */
interface Method {
  id: number;
  description: string
}

/**
 *Clase para la vista Servicios
 *
 * @export
 * @class ServicesPage
 */
@Component({
  selector: 'app-services',
  templateUrl: './services.page.html',
  styleUrls: ['./services.page.scss'],
})
export class ServicesPage {
  // Declarar variable method
  public selectedMethod: Method = {
    id: 1,
    description: 'CONSULTAR CONVENIO',
  };
  // Declarar variable guidConv
  public guidConv: string = "BFA3AB5F-BAEB-42DE-986E-0DA98F45F90F"
  // Declarar variable data
  public data: string = "IMEI:123;Modelo:123";
  // Declarar variable guidCiu
  public guidCiu: string = "3f200221-b9f8-4767-9a57-017974aa8b64";
  // Declarar variable dataOTP
  public dataOTP: string = "SMS";
  // Declarar variable message
  public message: string = "Es una prueba";
  // Declarar variable guidOTP
  public guidOTP: string = "a90eafae-f047-4aa9-bea9-37933ddd768e";
  // Declarar variable otp
  public otp: string = "6vDb";
  // Declarar variable tipoDoc
  public tipoDoc: string = "CC";
  // Declarar variable numDoc
  public numDoc: string = "1115080936";
  // Declarar variable email
  public email: string = "yeison.betancourt@cetoco.com";
  // Declarar variable celular
  public celular: string = "3162793738";
  // Declarar variable datosAdi
  public datosAdi: string = "";
  // Declarar variable procesoConvenioGuid
  public procesoConvenioGuid: string = "83EF0C90-E2C2-4593-A5D1-9597C3D50A3F";
  // Declarar variable datosAdi
  public asesor: string = "Prueba1";
  // Declarar variable datosAdi
  public sede: string = "000110";
  // Declarar variable texto
  public texto: string = "";
  // Declarar variable componente
  public componente: string = "";
  // Declarar variable usuario
  public usuario: string = "Prueba 7";
  // Declarar variable response
  public response: string = "";
  // Declarar variable loading
  public loading;
  // Declarar variable methods
  public methods: any[] = [
    {
      id: 1,
      description: 'CONSULTAR CONVENIO',
    },
    {
      id: 2,
      description: 'ENVIAR OTP',
    },
    {
      id: 3,
      description: 'VALIDAR OTP',
    },
    {
      id: 4,
      description: 'GUARDAR CIUDADANO',
    },
    {
      id: 5,
      description: 'CONSULTAR CIUDADANO',
    },
    {
      id: 6,
      description: 'GUARDAR LOG ERROR',
    },
  ];

  /**
   *Creates an instance of ServicesPage.
   * @param {LoadingController} loadingController
   * @param {NgZone} zone
   * @memberof ServicesPage
   */
  constructor(private zone: NgZone, private loadingController: LoadingController) {}

  /**
   *Función para validar cuando se habilita el botón guardar
   * @example
   * Simply call: 
   * validateButton()
   *
   * @memberof ServicesPage
   */
  validateButton(){
    switch (this.selectedMethod.id) {
      case 1:
        return this.guidConv.length === 0;
      case 2:
        return this.guidCiu.length === 0;
      case 3:
        return this.guidOTP.length === 0;
      case 4:
        return this.guidConv.length === 0;
      case 5:
        return this.guidCiu.length === 0;
      case 6:
        return this.guidConv.length === 0;
      default:
        return false;
    }
  }

  /**
   *Llama a las funciones según método
   * @example
   * Simply call: 
   * callAction()
   *
   * @memberof ServicesPage
   */
  callAction(){
    switch (this.selectedMethod.id) {
      case 1:
        this.callConsultarConvenio();
        break;
      case 2:
        this.callEnviarOTP();
        break;
      case 3:
        this.callValidarOTP();
        break;
      case 4:
        this.callGuardarCiudadano();
        break;
      case 5:
          this.callConsultarCiudadano();
        break;
      case 6:
          this.callGuardarlogError();
        break;
    }
  }

  /**
   *Función para llamar al método de consultarConvenio
   * @example
   * Simply call: 
   * callAction()
   *
   * @memberof ServicesPage
   */
  async callConsultarConvenio() {
    await this.presentLoading();
    this.response = "";
    //Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
    (<any>window).ReconoSerSdk.consultarConvenio(
      [this.guidConv, this.data],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   *Función para llamar al método de enviarOTP
   * @example
   * Simply call: 
   * callEnviarOTP()
   *
   * @memberof ServicesPage
   */
  async callEnviarOTP() {
    await this.presentLoading();
    this.response = "";
    //Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
    (<any>window).ReconoSerSdk.enviarOTP(
      [this.guidCiu, this.dataOTP, this.message],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   *Función para llamar al método de validarOTP
   * @example
   * Simply call: 
   * callValidarOTP()
   *
   * @memberof ServicesPage
   */
  async callValidarOTP() {
    await this.presentLoading();
    this.response = "";
    //Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
    (<any>window).ReconoSerSdk.validarOTP(
      [this.guidOTP, this.otp],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }
  
  /**
   *Función para llamar al método de guardarCiudadano
   * @example
   * Simply call: 
   * callGuardarCiudadano()
   *
   * @memberof ServicesPage
   */
  async callGuardarCiudadano() {
    await this.presentLoading();
    this.response = "";
    //Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
    (<any>window).ReconoSerSdk.guardarCiudadano(
      [this.guidConv, this.guidCiu, this.tipoDoc, this.numDoc, this.email, this.celular, this.datosAdi, this.procesoConvenioGuid, this.asesor, this.sede],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   *Función para llamar al método de consultarCiudadano
   * @example
   * Simply call: 
   * callConsultarCiudadano()
   *
   * @memberof ServicesPage
   */
  async callConsultarCiudadano() {
    await this.presentLoading();
    this.response = "";
    //Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
    (<any>window).ReconoSerSdk.consultarCiudadano(
      [this.guidCiu],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   *Función para llamar al método de guardarlogError
   * @example
   * Simply call: 
   * callGuardarlogError()
   *
   * @memberof ServicesPage
   */
  async callGuardarlogError() {
    await this.presentLoading();
    this.response = "";
    //Realiza el llamado al plugin e invoca segun el resultado la funcion correspondiente
    (<any>window).ReconoSerSdk.guardarlogError(
      [this.guidConv, this.texto, this.componente, this.usuario],
      this.successCallback.bind(this),
      this.errorCallback.bind(this)
    );
  }

  /**
   *Función para mostrar inicialiar loading
   * @example
   * Simply call: 
   * presentLoading()
   *
   * @memberof ServicesPage
   */
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Espere...',
    });
    await this.loading.present();
  }

  /**
   *Función para terminar loading
   * @example
   * Simply call: 
   * presentLoading()
   *
   * @memberof ServicesPage
   */
  async dismissLoading() {
    await this.loading.dismiss();
  }

  /**
   * Funcion para recibir las respuestas exitosas del SDK
   * 
   * @example
   * Simply call it with an object: 
   * successCallback({response, true})
   * 
   * @param {object} message
   * @memberof BiometricsPage
   */
  async successCallback(message) {
    this.zone.run(() => {
      if(typeof(message) === "object"){
        this.response = JSON.stringify(message);
      }else{
        this.response = message;
      }
    });
    await this.dismissLoading()
  }

  /**
   * Funcion para recibir las respuestas fallidas del SDK
   * 
   * @example
   * Simply call it with an object: 
   * errorCallback({error, true})
   * 
   * @param {object} err
   * @memberof BiometricsPage
   */
  async errorCallback(err) {
    this.zone.run(() => {
      if(typeof(err) === "object"){
        this.response = JSON.stringify(err);
      }else{
        this.response = err;
      }
    });
    await this.dismissLoading()
  }

  /**
   * Funcion para comparar objectos en selector
   * 
   * @example
   * Simply call with two arguments
   * compareFn(method1, method2)
   * 
   * @param {object} err
   * @memberof BiometricsPage
   * @returns boolean 
   */
  compareFn(e1: Method, e2: Method): boolean {
    return e1 && e2 ? e1.id == e2.id : e1 == e2;
  }
}
