# ReconoSer-Ionic

======

Aplicación de pruebas para SDK ReconoSer

======

## Iniciar ambiente de desarrollo

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.

## Listar plugin

`ionic cordova plugin list`

## Actualizar carpeta www for android

`ionic cordova prepare android`

## Actualizar carpeta www for iOS

`ionic cordova prepare iOS`

## Eliminar plugin SDK Reconoser

`cordova plugin remove cordova-plugin-reconoser`

## Instalar plugin SDK Reconoser

`cordova plugin add https://bitbucket.org/reconoser/cordova-plugin-reconoser.git`

## Autor

- **@betancourtYeison**